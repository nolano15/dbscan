/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4352
*
************************************************/

import java.util.*;

/**
 * Struct of TimeGene data objects
 */
public class TimeGene {
    private int id, grndtrth;
    private List<Double> data;
    private Set<TimeGene> nbhood;
    
    public TimeGene(int id) {
        this.id = id;
        data = new ArrayList<Double>(DBSCAN.N_TIME); // init to proper size
        nbhood = new HashSet<TimeGene>();
    }
    
    public void add(double d) {
        this.data.add(d);
    }

    public int getId() {
        return id;
    }

    public List<Double> getData() {
        // encapsulation
        return new ArrayList<Double>(data);
    }

    public int getGrndtrth() {
        return grndtrth;
    }

    public TimeGene setGrndtrth(int grndtrth) {
        this.grndtrth = grndtrth;
        return this;
    }

    public Set<TimeGene> getNbHood() {
        // encapsulation
        return new HashSet<TimeGene>(nbhood);
    }

    public void add(TimeGene tg) {
        this.nbhood.add(tg);
    }

    @Override
    public String toString() {
        return String.valueOf(id); // only need the ID
    }
}
/************************************************
*
* @author Nolan O'Shea
* Assignment: Prog 4
* Class: CS 4321
*
************************************************/

import java.io.*;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Contains DBSCAN algorithm and all necessary constants and functions
 */
public class DBSCAN {
    private static final String INPUT = "assignment4_input.txt";
    public static final int N = 500, // genes
                             N_TIME = 12; // time-points
    private List<TimeGene> tgList; // List of original data
    
    public DBSCAN() {
        this.tgList = load();
    }
    
    /**
     * Runs DBSCAN algorithm
     * 
     * @param e min radius of neighborhood
     * @param minPts min number of TimeGenes of neighborhood
     * 
     * @return Set of clusters
     */
    public Set<Set<TimeGene>> runDBSCAN(int e, int minPts) {
        // init neighborhoods
        for(int i = 0; i < N; i++) {
            // ternary operators help dodge case where TimeGene is added to its own neighborhood
            for(int j = i == 0 ? 1 : 0; j < N; j += j + 1 == i ? 2 : 1) {
                // neighbor test
                if(euclidean(tgList.get(i), tgList.get(j)) <= e) {
                    tgList.get(i).add(tgList.get(j));
                }
            }
        }
        
        var clusters = new HashSet<Set<TimeGene>>();
        
        // Select an arbitrary TimeGene
        // Set data structure should randomize the order
        for(TimeGene tg : new HashSet<TimeGene>(tgList)) {
            // If a core, then retrieve all TimeGenes density-reachable as a cluster
            if(tg.getNbHood().size() >= minPts) {
                clusters.add(density(tg));
            }
        }
        
        return clusters;
    }
    
    /**
     * Iteratively returns density reachable TimeGenes
     * 
     * @param tg TimeGene to start from
     * 
     * @return Set of density reachable TimeGenes
     */
    private Set<TimeGene> density(TimeGene tg) {
        Set<TimeGene> rchable = new HashSet<TimeGene>(),
                      rchableNext = tg.getNbHood(); // directly reachable
        
        // indirectly reachable
        while(!rchable.equals(rchableNext)) {
            rchable = new HashSet<TimeGene>(rchableNext);
            rchable.forEach(nb -> rchableNext.addAll(nb.getNbHood())); // Set will prevent dups
        }
        
        return rchable;
    }
    
    /**
     * Loads the data for all N TimeGenes into a List data structure
     * 
     * @return List of all loaded TimeGenes from the input file
     */
    private List<TimeGene> load() {
        // optimize: init to size N
        var tgList = new ArrayList<TimeGene>(N);
        
        try(var in = new Scanner(new BufferedInputStream(new FileInputStream(new File(INPUT))))) {
            // for all N genes
            for(int i = 0; i < N; i++) {
                // IDs will start at 1
                var tg = new TimeGene(i + 1)
                        .setGrndtrth(in.nextInt());
                
                // for each timepoint
                for(int j = 0; j < N_TIME; j++) {
                    // add to TimeGene's data
                    tg.add(in.nextDouble());
                }
                tgList.add(tg);
            }
        } catch(Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return tgList;
    }
    
    /**
     * Computes euclidean dist between a pair of TimeGenes
     * 
     * @param tg1 TimeGene to measure from
     * @param tg2 TimeGene to measure to
     * 
     * @return euclidean distance
     */
    private double euclidean(TimeGene tg1, TimeGene tg2) {
        double sum = 0;
        
        for(int i = 0; i < N_TIME; i++) {
            sum += Math.pow(tg1.getData().get(i) - tg2.getData().get(i), 2);
        }
        
        return round(Math.sqrt(sum));
    }
    
    /**
     * Rounds a double to 4 decimal places
     * 
     * @param d double to be rounded
     * 
     * @return rounded double
     */
    public static double round(double d) {
        DecimalFormat df = new DecimalFormat("#.####"); // format to 4 decimal places
        // HALF_EVEN will balance when to round up or down on *.5
        df.setRoundingMode(RoundingMode.HALF_EVEN);
        
        return Double.parseDouble(df.format(d));
    }

    public List<TimeGene> getTgList() {
        // encapsulation
        return new ArrayList<TimeGene>(tgList);
    }
}
